package android.burkb.crystalball;

public class Predictions {
    private static Predictions predictions;
    private String[] answers;

    private Predictions(){
answers = new String[] {
        "       Your wishes will come true.       ",
        "        You're hopeless, I'm done here!         ",
        "Your wishes will NEVER come true! HA HA HA!",
        "Something bad is going to happen to you within this hour",

};
    }

    public static Predictions get(){
        if(predictions == null){
            predictions = new Predictions();
        }
        return predictions;
    }
    public String getPrediction(){
        int range = (answers.length -1);
        int rand = (int) (Math.random()* range) + 1;
        return answers[rand];
    }
}

